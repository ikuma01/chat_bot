from django.test import TestCase
from django.utils import timezone
import datetime
from django.contrib.auth.models import User
from django.test import Client

# url関係のテスト
from django.shortcuts import resolve_url
from django.urls import reverse
from django.contrib.auth import views as auth_views




# 画面に行った際に正しく表示されるかテスト
class ScreenTransition(TestCase):
    def logIn(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def logOut(self):
         url = reverse('logout')
         response = self.client.get(url)
         self.assertEquals(response.status_code, 200)


    def passwordChange(self):
        url = reverse('password_change')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def passwordChangeDone(self):
         url = reverse('password_change_done')
         response = self.client.get(url)
         self.assertEquals(response.status_code, 200)


    def passwordReset(self):
        url = reverse('password_reset')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def passwordResetDone(self):
         url = reverse('password_reset_done')
         response = self.client.get(url)
         self.assertEquals(response.status_code, 200)

    def passwordResetConfirm(self):
        url = reverse('password_reset_confirm')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def passwordResetComplete(self):
         url = reverse('password_reset_complete')
         response = self.client.get(url)
         self.assertEquals(response.status_code, 200)
