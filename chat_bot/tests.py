from django.test import TestCase
from django.utils import timezone
import datetime
from django.contrib.auth.models import User
from django.test import Client
from .models import Message

# url関係のテスト
from django.shortcuts import resolve_url
from django.urls import reverse


# chat画面に行った際に正しく表示されるかテスト
class ChatScreenTransition(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('ikuma', 'math1123@gmail.com', 'math1123')

    def testLogin(self):
        self.client.login(username='ikuma', password='math1123')
        url = reverse('chat_bot_message')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

