from django import forms
from .models import Message

    
class CheckForm(forms.Form):
    empty = forms.CharField(label='Empty', empty_value=True)

class MessageForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['content']
