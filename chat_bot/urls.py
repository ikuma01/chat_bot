from django.urls import path
from . import views

urlpatterns = [
    path('', views.message, name='chat_bot_message'),
]
