from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from .models import Message
from .forms import  MessageForm, CheckForm
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required

import pickle
from gensim.models.doc2vec import Doc2Vec, TaggedDocument

import MeCab
wakati = MeCab.Tagger("-Owakati")

with open('./ml_models/old_documents.pkl', 'rb') as f:
    documents = pickle.load(f)
with open('./ml_models/old_model.pkl', 'rb') as f:
    model = pickle.load(f)
with open('./ml_models/val_evaluation.pkl', 'rb') as f:
    val_evaluation = pickle.load(f)
with open('./ml_models/reply_ms.pkl', 'rb') as f:
    reply_ms = pickle.load(f)


@login_required
def message(request):
    if (request.method == 'POST'):
        #DBに保存
        obj = Message()
        obj.owner_id = request.user.id
        obj.respond = bot_respond(request.POST['content'])
        form = MessageForm(request.POST, instance=obj)
        form.save()

    data = Message.objects.filter(owner_id=request.user.id)
    
    params = {
        'form' : MessageForm(),
        'data': data,
        }
    return render(request, 'chat_bot/index.html', params)




# def bot_respond(msg):
#     return '貴方は言いました。「' + msg + '」と。'

# ---------------------------------------------------------------------------------------
def bot_respond(text):
  text_wakati = wakati.parse(text).split()
  max_value = 0
  max_evaluation = 0
  max_evaluation_num = 0

  for i in range(len(documents)):
    sim_value = model.docvecs.similarity_unseen_docs(model, text_wakati, documents[i][0], alpha=1, min_alpha=0.0001, steps=5)
    if max_value < sim_value:
      max_value = sim_value
      max_num = i
  
  temp = str(documents[max_num][1])
  temp = temp.replace("['", '')
  temp = int(temp[:temp.find('_')])

  for i in range(len(val_evaluation[0])):
    if max_evaluation < val_evaluation[temp][i]:
      max_evaluation = val_evaluation[temp][i]
      max_evaluation_num = i
  return reply_ms[max_evaluation_num]
