from django.contrib import admin
from django.urls import path,include

urlpatterns = [
    path('admin', admin.site.urls),#
    path('chat', include('chat_bot.urls')),

    # 将来的にはアカウントアプリケーションで行う
    path('', include('accounts.urls')),
    # path('test', include('django.contrib.auth.urls')),
]
