# Requirement
1: Bootstrap v4.5.2
2: fontawesome-free-5.14.0-web
3: jquery-3.5.1


# Installation
1: https://getbootstrap.com/docs/4.5/getting-started/download/
2: https://fontawesome.com/download
3: https://jquery.com/download/
4: https://sendgrid.com/